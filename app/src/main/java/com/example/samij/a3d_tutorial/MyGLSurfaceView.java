package com.example.samij.a3d_tutorial;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by Samij on 31.1.2017.
 */
public class MyGLSurfaceView extends GLSurfaceView {

    private final MyGLRenderer mRenderer;

    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float mPreviousX;
    private float mPreviousY;
    private boolean isOnClick;

    public MyGLSurfaceView(Context context) {
        super(context);

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        mRenderer = new MyGLRenderer(context);

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data.
        // To allow the triangle to rotate automatically, this line is commented out:
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.

        float x = e.getX();
        float y = e.getY();
        final float SCROLL_THRESHOLD = 3;

        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                isOnClick = true;

                break;

            case MotionEvent.ACTION_UP:
                if (isOnClick) {
                    setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
                    mRenderer.setAutoRotate();
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (isOnClick && (Math.abs(mPreviousX - e.getX()) > SCROLL_THRESHOLD ||
                                Math.abs(mPreviousY - e.getY()) > SCROLL_THRESHOLD)) {
                    isOnClick = false;
                }

                setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

                float dx = x - mPreviousX;
                float dy = y - mPreviousY;

                // reverse direction of rotation above the mid-line
                if (y > getHeight() / 2) {
                    dx = dx * -1 ;
                }

                // reverse direction of rotation to left of the mid-line
                if (x < getWidth() / 2) {
                    dy = dy * -1 ;
                }

                mRenderer.setAngle(
                        mRenderer.getAngle() +
                                ((dx + dy) * TOUCH_SCALE_FACTOR));
                requestRender();
                break;

        }

        mPreviousX = x;
        mPreviousY = y;
        return true;
    }
}
