package com.example.samij.a3d_tutorial;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Samij on 31.1.2017.
 */

public class Pyramid implements ShapeIF {

    private final String vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 a_Position;" +   // Per-vertex position information we will pass in.
                    "attribute vec4 a_Color;" +     // Per-vertex color information we will pass in.
                    "varying vec4 vColor;" +       // This will be passed into the fragment shader.

                    "void main() {" +

                    "   vColor = a_Color;"	+	// Pass the color through to the fragment shader.

                    // the matrix must be included as a modifier of gl_Position
                    // Note that the uMVPMatrix factor *must be first* in order
                    // for the matrix multiplication product to be correct.
                    "  gl_Position = uMVPMatrix * a_Position;" +
                    "}";

    private final String fragmentShaderCode =
            "precision mediump float;" + // Set the default precision to medium. We don't need as high of a precision in the fragment shader.
                    "varying vec4 vColor;" + // This is the color from the vertex shader interpolated across the triangle per fragment.
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    // Use to access and set the view transformation
    private int mMVPMatrixHandle;

    private FloatBuffer vertexBuffer;
    private FloatBuffer colorBuffer;
    private ShortBuffer drawListBuffer;

    private final int mProgram;
    private int mPositionHandle;
    private int mColorHandle;

    private final int vertexCount = pyramidCoords.length / COORDS_PER_VERTEX;
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    private final int colorCount = pyramidColors.length / COLORS_PER_VERTEX;
    private final int colorStride = COLORS_PER_VERTEX * 4; // 4 bytes per color

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static float pyramidCoords[] = {
            0.0f,  0.5f, 0.0f,   //     top                 0
            -0.5f, -0.5f, 0.5f,   //    bottom front-left   1
            0.5f, -0.5f, 0.5f,   //     bottom front-right  2
            -0.5f, -0.5f, -0.5f,   //   bottom back-left    3
            0.5f, -0.5f, -0.5f };   //  bottom back-right   4

    // Set color with red, green, blue and alpha (opacity) values
    static final int COLORS_PER_VERTEX = 4;
    static float pyramidColors[] = {
            // Front face (red)
            1.0f, 0.0f, 0.0f, 1.0f};

    // order to draw vertices
    private short drawOrder[] = {
            0, 1, 2,            //Front
            0, 2, 4,            //Right
            0, 4, 3,            //Back
            0, 3, 1            //Left
            //1, 3, 4, 1, 4, 2    //Bottom
    };

    public Pyramid(){
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 4 bytes per float)
                pyramidCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(pyramidCoords);
        vertexBuffer.position(0);

        // initialize byte buffer for the draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 2 bytes per short)
                drawOrder.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

        // initialize byte buffer for the colors
        ByteBuffer cbb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 4 bytes per float)
                pyramidColors.length * 4);
        cbb.order(ByteOrder.nativeOrder());
        colorBuffer = cbb.asFloatBuffer();
        colorBuffer.put(pyramidColors);
        colorBuffer.position(0);

        /*
           Note: Compiling OpenGL ES shaders and linking programs is expensive in terms of CPU cycles and processing time,
           so you should avoid doing this more than once. If you do not know the content of your shaders at runtime,
           you should build your code such that they only get created once and then cached for later use.
        */
        int vertexShader = MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
                vertexShaderCode);
        int fragmentShader = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderCode);

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();

        // add the vertex shader to program
        GLES20.glAttachShader(mProgram, vertexShader);

        // add the fragment shader to program
        GLES20.glAttachShader(mProgram, fragmentShader);

        //Bind attributes
        //GLES20.glBindAttribLocation(mProgram, 0, "a_Position");
        //GLES20.glBindAttribLocation(mProgram, 1, "a_Color");

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(mProgram);
    }


    @Override
    public void draw(float[] mvpMatrix) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "a_Position");

        // Enable a handle to the pyramid vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the pyramid coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        // get handle to fragment shader's a_Color member
        mColorHandle = GLES20.glGetAttribLocation(mProgram, "a_Color");

        // Enable a handle to the pyramid color
        GLES20.glEnableVertexAttribArray(mColorHandle);

        // Prepare the pyramid color data
        GLES20.glVertexAttribPointer(mColorHandle, COLORS_PER_VERTEX,
                GLES20.GL_FLOAT, false, colorStride, colorBuffer);

        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);

        // Draw the pyramid
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
                GLES20.GL_UNSIGNED_SHORT, drawListBuffer);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }
}
