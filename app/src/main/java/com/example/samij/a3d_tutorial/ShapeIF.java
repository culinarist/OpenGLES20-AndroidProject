package com.example.samij.a3d_tutorial;

/**
 * Created by Samij on 31.1.2017.
 */

public interface ShapeIF {

    public void draw(float[] mvpMatrix);
}
