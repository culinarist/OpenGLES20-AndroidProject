package com.example.samij.a3d_tutorial;

import android.opengl.GLES20;

/**
 * Created by Samij on 8.2.2017.
 *
 */

public class SimpleLight {

    /** This will be used to pass in the light position. */
    private int mLightPosHandle;

    /** This will be used to pass in the transformation matrix. */
    private int mMVPMatrixHandle;

    private final int mProgram;

    final String pointVertexShaderCode =
            "uniform mat4 u_MVPMatrix;      \n"
                    +	"attribute vec4 a_Position;     \n"
                    + "void main()                    \n"
                    + "{                              \n"
                    + "   gl_Position = u_MVPMatrix   \n"
                    + "               * a_Position;   \n"
                    + "   gl_PointSize = 5.0;         \n"
                    + "}                              \n";

    final String pointFragmentShaderCode =
            "precision mediump float;       \n"
                    + "void main()                    \n"
                    + "{                              \n"
                    + "   gl_FragColor = vec4(1.0,    \n"
                    + "   1.0, 1.0, 1.0);             \n"
                    + "}                              \n";

    public SimpleLight(){

        int vertexShader = MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
                pointVertexShaderCode);
        int fragmentShader = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
                pointFragmentShaderCode);

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();

        // add the vertex shader to program
        GLES20.glAttachShader(mProgram, vertexShader);

        // add the fragment shader to program
        GLES20.glAttachShader(mProgram, fragmentShader);

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(mProgram);

    }

    public void drawLight(float[] mvpMatrix, float[] posInModelSpace)
    {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);

        mLightPosHandle = GLES20.glGetAttribLocation(mProgram, "a_Position");
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");

        // Pass in the position.
        GLES20.glVertexAttrib3f(mLightPosHandle, posInModelSpace[0], posInModelSpace[1], posInModelSpace[2]);

        // Since we are not using a buffer object, disable vertex arrays for this attribute.
        GLES20.glDisableVertexAttribArray(mLightPosHandle);

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);

        // Draw the point.
        GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);
    }

}
